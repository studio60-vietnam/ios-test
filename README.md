Please create a simple application only 1 screen with these following requirements:

* create new branch with your name
* pull to reload
* call api and parse Json
* UI: the same with image, please download

This is link UI: https://www.dropbox.com/s/zfb3ut5uedjn8qk/interview.png?dl=0

Information of item in UI:

- color of navigation bar : #004390
- background color of bar bottom:   #000000 60%
- background color of date bottom: #000000 60%
- font size of Title: helvetica white 15px
- font size of desc: helvetica white 12px
- date bar: top 22 right 0 width 80 height 20
- height bar bottom: 49px

API: we call api in link: http://api.nytimes.com/svc/topstories/v1/home.json?api-key=1720fdd7b0f8ca794fa043a04ff79b99:12:74602315
we show information mapping with key:

Title: Title
Abstract: Abstract
Image: url
